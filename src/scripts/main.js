var appModule = angular.module("spaApp", ["ngRoute","firebase"]);


/*var appModule = angular.module('myApp', ['firebase']);*/

appModule.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/profile/', {
			templateUrl: 'templates/profile.html',
			controller: 'profileController'
		})

		.when('/service', {
			templateUrl: 'templates/service.html',
			controller: 'serviceController'
		})

		.when('/booking', {
			templateUrl: 'templates/booking.html',
			controller: 'bookingController'
		})

		.when('/member', {
			templateUrl: 'templates/member.html',
			controller: 'memberController'
		})
}]);