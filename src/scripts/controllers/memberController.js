appModule.controller("memberController", function($scope, $http) {
	
	$http({
        method : "GET",
        url : "http://swapi.co/api/people/?page=1"
    }).then(function mySucces(response) {
    	console.log(response.data);
        $scope.data = response.data;
    }, function myError(response) {
        //$scope.myWelcome = response.statusText;
    });

    $scope.pagePrev = function() {
    	if($scope.data.previous === null)
    		return;

    	$http({
	        method : "GET",
	        url : $scope.data.previous
	    }).then(function mySucces(response) {
	    	console.log(response.data);
	        $scope.data = response.data;
	    }, function myError(response) {
	      	console.log(response.statusText);
	    });
    }

    $scope.pageNext = function() {
    	if($scope.data.next === null)
    		return;

    	$http({
	        method : "GET",
	        url : $scope.data.next
	    }).then(function mySucces(response) {
	    	console.log(response.data);
	        $scope.data = response.data;
	    }, function myError(response) {
	        console.log(response.statusText);
	    });
    }

});