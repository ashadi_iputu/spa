appModule.controller("profileController", function($scope, $routeParams, $firebaseObject, $sce) {
	var ref = new Firebase("https://spa-project-ba685.firebaseio.com/profile");
	var syncObject = $firebaseObject(ref);
	syncObject.$bindTo($scope, "profile");

	$scope.$sce = $sce;
});