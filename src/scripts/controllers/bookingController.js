appModule.controller("bookingController", function($scope, $firebaseObject) {
	var ref = new Firebase("https://spa-project-ba685.firebaseio.com/booking");
	var syncObject = $firebaseObject(ref);
	syncObject.$bindTo($scope, "booking");
});