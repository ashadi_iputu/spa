var gulp = require('gulp'),
	watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
	sass = require('gulp-sass'),
	cssnano = require('gulp-cssnano');


var srcFolder = './src',
	destFolder = './dist';

var assets = {
				source : {
					sass: [
							srcFolder + '/sass/*.scss'
							],
					styles: [
								'./bower_components/bootstrap/dist/css/bootstrap.min.css',
								srcFolder + '/styles/*.css'
							],
					scripts: [
								'./bower_components/angular/angular.js',
								'./bower_components/angular-route/angular-route.js',
								'./bower_components/jquery/dist/jquery.js',
								'./bower_components/bootstrap/dist/js/bootstrap.js',
								srcFolder + '/scripts/**/*.js'
							],
					fonts: [
								'./bower_components/bootstrap/fonts/*'
							],
					images: [
								srcFolder + '/images/**/*'
							]
				},
				destination : {
					sass: srcFolder + '/styles/',
					scripts: destFolder + '/scripts/',
					styles: destFolder + '/styles/',
					fonts: destFolder + '/fonts/',
					images: destFolder + '/images/'
				},
				file : {
					css: 'style.css',
					js: 'script.js'
				}
			};

gulp.task('default', ['sass-task', 'style-task', 'script-task', 'image-min-task'] ,function(){
	watch(assets.source.sass, function(){
		gulp.start('sass-task')
	});

	watch(assets.source.styles, function(){
		gulp.start('style-task')
	});

	watch(assets.source.scripts, function(){
		gulp.start('script-task')
	});

	watch(assets.source.images, function(){
		gulp.start('image-min-task')
	});
});

gulp.task('image-min-task', function(){
	gulp.src(assets.source.images)
		.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
		.pipe(gulp.dest(assets.destination.images));
});

gulp.task('sass-task', function(){
	gulp.src(assets.source.sass)
		.pipe(sass({outputStyle: 'expanded'}))
	    .pipe(plumber({
	     	errorHandler: function (error) {
	        console.log(error.message);
	        this.emit('end');
	    }}))
	    .pipe(autoprefixer('last 2 versions'))
	    .pipe(gulp.dest(assets.destination.sass))
});

gulp.task('style-task', function(){
	gulp.src(assets.source.styles)
		.pipe(concat(assets.file.css))
		.pipe(cssnano({
			discardComments: { removeAll: true }
		}))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(assets.destination.styles))
});

gulp.task('script-task', function(){
 	gulp.src(assets.source.scripts)
 		.pipe(concat(assets.file.js))
 		.pipe(rename({suffix: '.min'}))
 		//.pipe(uglify())
 		.pipe(gulp.dest(assets.destination.scripts))
});